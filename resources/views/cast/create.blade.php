@extends('adminlte.master')
@section('title')
  <h2>Create Table</h2>
@endsection
@section('content')
<form method="POST" action="/cast">
  @csrf
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama','')}}" placeholder="Enter your name">
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur','')}}" placeholder="Age">
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="bio">Bio</label>
    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio','')}}">
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection