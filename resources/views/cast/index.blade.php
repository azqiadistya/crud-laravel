@extends('adminlte.master')
@section('title')
  <h2>Data Master</h2>
@endsection
@section('content')
<table class="table">
  
    @if(session('success'))
      <div class="alert alert-success">
        {{session('success')}}
      </div>
    @endif
  
    <a type="button" class="btn btn-primary mb-3 mt-2" href="/cast/create">+ data</a>
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($casts as $key => $cast)
    <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $cast->nama }}</td>
        <td>{{ $cast->umur }}</td>
        <td>{{ $cast->bio }}</td>
        <td style="display:flex;">
            <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm ml-1">show</a>
            <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm ml-1">Edit</a>
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf 
                @method('DELETE')
                <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1">
            </form>
        </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection