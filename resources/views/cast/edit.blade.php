@extends('adminlte.master')
@section('title')
  <h2>Edit Data {{$casts->id}}</h2>
@endsection
@section('content')
<form method="POST" action="/cast/{{$casts->id}}">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama',$casts->nama)}}" placeholder="Enter your name">
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur',$casts->umur)}}" placeholder="Age">
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="bio">Bio</label>
    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio',$casts->bio)}}">
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  </div>
  <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection