<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view ('cast.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect ('/cast')->with('success','Data berhasil disimpan!!');
    }

    public function index(){
        $casts = DB::table('cast')->get();
        // dd($cast);
        return view ('cast.index', compact('casts'));
    }
    public function show($id){
        $casts = DB::table('cast')->where('id',$id)->first();
        // dd($cast);
        return view ('cast.show', compact('casts'));
    }
    public function edit($id){
        $casts = DB::table('cast')->where('id',$id)->first();
        // dd($cast);
        return view ('cast.edit', compact('casts'));
    }
    public function update($id, Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $casts = DB::table('cast')
                    ->where('id',$id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);
        // dd($cast);
        return redirect('/cast')->with('success','Berhasil Update Data!');
    }
    public function destroy($id){
        // dd($request->all());
        $query = DB::table('cast')->where('id',$id)->delete();
        return redirect ('/cast')->with('success','Data berhasil diHAPUS!!');
    }


}
